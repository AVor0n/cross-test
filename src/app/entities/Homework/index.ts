export { homeworkActions, homeworkReducer } from './slice/Homework.slice';
export * from './services';
export type { HomeworkSchema } from './types';

export * from './selectors/getHomeworkData';
